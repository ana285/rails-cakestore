class User < ApplicationRecord
  validates :full_name, presence: true
  validates :full_name, length: { minumum:2, maximum: 30 }

  validates :password, :presence =>true,
            :length => { :minimum => 6, :maximum => 40 },
            :confirmation =>true
  #validates_presence_of :password_confirmation, if: :password_changed?

  validates :telephone, :presence => true
  validates :telephone, length: {minimum: 10, maximum: 20}

  email_regex = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  validates :email, :presence => true,
            :format => { :with => email_regex },
            :uniqueness => { :case_sensitive => false }
end

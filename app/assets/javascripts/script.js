var vect=[];


function loadMenu(session){
    $.ajax({
        type:"GET",
        url:"loadMenu",
        dataType:"json",
        success:function(result) {

            console.log(result);
            var raspuns = result.file_content.products;
            for(var i = 0; i < raspuns.length; ++i){
                console.log(raspuns[i].name);
                var tip = raspuns[i].food_type;

                vect[i] = "<p>";
                for(var j = 0; j < raspuns[i].ingredients.length;++j)
                {
                    console.log(raspuns[i].ingredients[j].name);
                    vect[i] += "&emsp;&emsp;&emsp;" + raspuns[i].ingredients[j].name + "<br\>";
                }
                vect[i] += "</p>";

                console.log(vect[i]);
                console.log(tip+"_div");
                if(session != null) {
                    document.getElementById(tip + "_div").innerHTML +=
                        '<div class="col-sm-4 thumb">' +
                        '<div class="thumbnail">' +
                        '<img src="' + "../assets" + raspuns[i].image_path.replace("images/", "") + '" alt="' + raspuns[i].name + '">' +
                        '<div class="caption">' +
                        '<h4 class="pull-right">' + raspuns[i].price + '$</h4>' +
                        '<h4 style="color: #337AB7">' + raspuns[i].name + '</h4>' +
                        '<div class="btnProd">' +
                        '<button id="myBtn' + i + '" class="btn3" onclick="afisareDetaliiProduse(' + i + ')">MORE</button>' +
                        '<form class="addToCart" action="addCart" method="POST">' +
                        '<button id="AddButton' + i + '" name="AddButton" value="' + raspuns[i].name + '" class="btn3" type="submit"> ADD&nbsp;<span ' +
                        '               class="glyphicon glyphicon-shopping-cart"></span></button>' +
                        '</form>' +
                        '</div>' +
                        '</div>' +
                        '<div id="myModal' + i + '" class="modal">' +
                        '<div class="modal-content">' +
                        '<div class="modal-header">' +
                        '<span class="close">&times;</span>' +
                        '</div>' +
                        '<div class="modal-body">' +
                        '<div class="container5">' +
                        '<div class="left">' +
                        '<img id = "more" class = "thumbnail" src="' + "../assets" + raspuns[i].image_path.replace("images/", "") + '" alt="' + raspuns[i].name + '">' +
                        '</div>' +
                        '<div class="right">' +
                        '<br />' +
                        '<p style="font-family: Montsserrat; text-align: center; font-style: italic; font-size: 1.7em;">' +
                        raspuns[i].name + '</p>' +
                        '<br /> <br />' +
                        '<p>&nbsp; Ingredients:</p>' +
                        vect[i] + '' +
                        '</div>' +

                        '</div>' +
                        '</div>' +
                        '</div>' +
                        '</div>' +
                        '</div>' +
                        '</div>';
                }else{
                    document.getElementById(tip + "_div").innerHTML +=
                        '<div class="col-sm-4 thumb">' +
                        '<div class="thumbnail">' +
                        '<img src="' + "../assets" + raspuns[i].image_path.replace("images/", "") + '" alt="' + raspuns[i].name + '">' +
                        '<div class="caption">' +
                        '<h4 class="pull-right">' + raspuns[i].price + '$</h4>' +
                        '<h4 style="color: #337AB7">' + raspuns[i].name + '</h4>' +
                        '<div class="btnProd">' +
                        '<button id="myBtn' + i + '" class="btn3" onclick="afisareDetaliiProduse(' + i + ')">MORE</button>' +
                        '</div>' +
                        '</div>' +
                        '<div id="myModal' + i + '" class="modal">' +
                        '<div class="modal-content">' +
                        '<div class="modal-header">' +
                        '<a href="#" onclick="return false" >'+
                        '<span onmouseup="hide()" class="close">&times;</span> </a>' +
                        '</div>' +
                        '<div class="modal-body">' +
                        '<div class="container5">' +
                        '<div class="left">' +
                        '<img id = "more" class = "thumbnail" src="' + "../assets" + raspuns[i].image_path.replace("images/", "") + '" alt="' + raspuns[i].name + '">' +
                        '</div>' +
                        '<div class="right">' +
                        '<br />' +
                        '<p style="font-family: Montsserrat; text-align: center; font-style: italic; font-size: 1.7em;">' +
                        raspuns[i].name + '</p>' +
                        '<br /> <br />' +
                        '<p>&nbsp; Ingredients:</p>' +
                        vect[i] + '' +
                        '</div>' +

                        '</div>' +
                        '</div>' +
                        '</div>' +
                        '</div>' +
                        '</div>' +
                        '</div>';
                }
            }
        }
    });

}

function loadMenuIndex()
{
    $.ajax({
        type:"GET",
        url:"loadMenu",
        dataType:"json",
        success:function(result){
            console.log(result);
            var raspuns = result.file_content.products;
            for(var i = 0; i < 6; ++i){
                console.log(raspuns[i].name);
                var tip = raspuns[i].type;

                vect[i] = "<p>";
                for(var j = 0; j < raspuns[i].ingredients.length;++j)
                {
                    console.log(raspuns[i].ingredients[j].name);
                    vect[i] += "&emsp;&emsp;&emsp;" + raspuns[i].ingredients[j].name + "<br\>";
                }
                vect[i] += "</p>";

                console.log(vect[i]);

                document.getElementById("Index_div").innerHTML +=
                    '<div class="col-sm-4 thumb">'+
                		'<div class="thumbnail">'+
                			'<a href="menu"><img src="'+"assets" + raspuns[i].image_path.replace("images/", "") +'" alt="'+raspuns[i].name+'"></a>'+
                			'	<div class="caption">'+
                					'<h4 class="pull-right">'+raspuns[i].price+'$</h4>'+
                					'<h4 style="color: #337AB7">'+raspuns[i].name+'</h4>'+
                				'</div>'+
                				'<div id="myModal'+ i +'" class="modal">'+
                					'<div class="modal-content">'+
                						'<div class="modal-header">'+
                							'<span class="close">&times;</span>'+
                						'</div>'+

                					'</div>'+
                				'</div>'+
                			'</div>'+
                		'</div>'+
                 	'</div>';
            }

        }
    })
}



function afisareDetaliiProduse(id){
	
	var modal = document.getElementById('myModal' + id);
	var btn = document.getElementById('myBtn'+id);
	var span = document.getElementsByClassName('close')[id];

	console.log(span);
	var ok=false;
	
	btn.onclick = function() {
		modal.style.display = "block";
	};
	
	console.log('da');
    span.addEventListener("click", function() {
        console.log('apasat buton x');

        modal.style.display = "none";
		ok=true;
	});
	
	console.log('daa');
	window.onclick = function(event) {
		if(ok==false)
			modal.style.display="block";
		if (event.target == modal) {
			modal.style.display = "none";
		}
	}
}



function afisareDetaliiComanda(){
	
	var modal = document.getElementById('myModal');
	var btn = document.getElementById("btn");
	var span = document.getElementsByClassName("close")[0];
	console.log('da');
	var ok = false;

	btn.onclick = function() {
	    modal.style.display = "block";
	};

	span.onclick = function() {
	    modal.style.display = "none";
	    ok=true;
	};

	window.onclick = function(event) {
		if(ok==false)
			modal.style.display="block";
	    if (event.target == modal) {
	        modal.style.display = "none";
	    }
	}
}

var count=0;

function adauga(id){
	var btn = document.getElementById('AddButton'+id);
	console.log(count);
}




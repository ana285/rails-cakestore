class SessionsController < ApplicationController
  #skip_before_action :authorize
  #
  skip_before_action :verify_authenticity_token

  def new
  end

  def create
    user = User.find_by(email: params[:email])
    if user and user.password == params[:password]
      session[:user_id] = user.id
      session[:cart] ||= {}
      redirect_to root_url
    else
      redirect_to login_url, alert:"Invalid username or password"
    end
  end

  def destroy
    session[:user_id] = nil
    session[:cart] = nil
    session.clear
    redirect_to login_url, alert:"Successfully logges out"
  end

  def mycart

  end

  def addToCart
    prod = params[:AddButton]
    plsql.connection = OCI8.new('c##ecbd/parola@//localhost:1521/orcl')

    if session[:cart][prod] != nil
      @result =  plsql.menu.verify_ingredients_quantity(params[:AddButton], session[:cart][prod] + 1)
      puts @result
      if @result[:response] == true
        session[:cart][prod] += 1
        puts 'produs existent'
        puts session[prod]
        puts session[:cart]
        session[:cart]['total'] += session[prod]['price']
      else

      end
      puts session[:cart]
    else
      @result =  plsql.menu.verify_ingredients_quantity(params[:AddButton], 1)
      puts @result
      @product = plsql.menu.get_product(params[:AddButton])
      puts @product

      if @result[:response] == true
        session[:cart][prod] = 1
        if session[:cart]['total'] == nil
          session[:cart]['total'] = 0
        end
        session[:cart]['total'] += @product[:product][:price]
        session[prod] ||= {}
        session[prod][:name] = @product[:product][:name]
        session[prod][:price] = @product[:product][:price]
        session[prod][:img] = @product[:product][:image_path]
        puts session[prod]
      else
        @message = "We don't have the ingredients"
      end
    end
    puts session[:cart]
    respond_to do |format|
      if @result[:response] == true
        format.html { redirect_to menu_url, notice: 'Product was successfully added.' }
        #format.json { render :show, status: :created, location: @user }
      else
        format.html { redirect_to menu_url, notice: 'Product was not added. We have not enough infredients :(' }
        #format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end



  def plusProduct

    prod = params[:AddButton]
    plsql.connection = OCI8.new('c##ecbd/parola@//localhost:1521/orcl')

    if session[:cart][prod] != nil
      @result =  plsql.menu.verify_ingredients_quantity(params[:AddButton], session[:cart][prod] + 1)
      puts @result
      if @result[:response] == true
        session[:cart][prod] += 1
        puts 'produs existent'
        puts session[prod]
        puts session[:cart]
        session[:cart]['total'] += session[prod]['price']
      else

      end
      puts session[:cart]
    else
      @result =  plsql.menu.verify_ingredients_quantity(params[:AddButton], 1)
      puts @result
      @product = plsql.menu.get_product(params[:AddButton])
      puts @product

      if @result[:response] == true
        session[:cart][prod] = 1
        if session[:cart]['total'] == nil
          session[:cart]['total'] = 0
        end
        session[:cart]['total'] += @product[:product][:price]
        session[prod] ||= {}
        session[prod][:name] = @product[:product][:name]
        session[prod][:price] = @product[:product][:price]
        session[prod][:img] = @product[:product][:image_path]
        puts session[prod]
      else
        @message = "We don't have the ingredients"
      end
    end
    puts session[:cart]
    respond_to do |format|
      if @result[:response] == true
        format.html { redirect_to cart_url }
        #format.json { render :show, status: :created, location: @user }
      else
        format.html { redirect_to cart_url, notice: 'Product was not added. We have not enough infredients :(' }
        #format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  def minusProduct
    prod = params[:Minus]

    if session[:cart][prod] != nil
      if session[:cart][prod] - 1 > 0
        session[:cart][prod] -= 1
        session[:cart]['total'] -= session[prod]['price']
      elsif session[:cart][prod] - 1 == 0
        session[:cart].delete(prod)
        puts session[:cart]
        session[:cart]['total'] -= session[prod]['price']
        session[prod] = nil
      end

    end

    puts session[:cart]
    respond_to do |format|
        format.html { redirect_to cart_url }
      end

  end

  def removeProductFromCart
    prod = params[:Remove]
    if session[:cart][prod] != nil
      session[:cart]['total'] -= session[:cart][prod] *
          session[prod]['price']
      session[:cart].delete(prod)
      puts session[:cart]
      session[prod] = nil
    end

    puts session[:cart]
    respond_to do |format|
      format.html { redirect_to cart_url }
    end
  end


  def testare
     data = [{:x => 1, :y => 2}, {:x => 3, :y => 4}]

    plsql.connection = OCI8.new('c##ecbd/parola@//localhost:1521/orcl')
    @result = plsql.add_points(data)
    puts @result
  end

  def postOrder
    plsql.connection = OCI8.new('c##ecbd/parola@//localhost:1521/orcl')

    data = []
    session[:cart].each {|product, number|
      if product != 'total'
        data.push({:name => product, :no => number})
      end
    }

    puts data

    @result = plsql.order_package.placeOrder(data, session[:user_id])
    puts @result


    session[:cart].each {|product, number|
      session[product] = nil
    }
    session[:cart] = nil
    session[:cart] ||= {}


    respond_to do |format|
        if @result == true
          format.html { redirect_to cart_url, notice: 'The order was successfully registered!' }
        else
          format.html { redirect_to cart_url, notice: 'There was some problems. The order was ot registered :(' }
        end
      end

  end


end

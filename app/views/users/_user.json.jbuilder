json.extract! user, :id, :full_name, :email, :password, :telephone, :address1, :address2, :created_at, :updated_at
json.url user_url(user, format: :json)

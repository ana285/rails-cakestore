Rails.application.routes.draw do

  get 'menu/index'
  root 'welcome#index'
  get 'welcome/index'
  get 'welcome/about'
  get 'welcome/contact'
  get 'admin' => 'admin#index'
  get 'login' => 'sessions#new'
  post 'login' => 'sessions#create'
  get 'register' => 'users#new'
  get 'cart' => 'sessions#mycart'
  get 'account' => 'users#show'
  get 'logout' => 'sessions#destroy'
  get 'loadMenu' => 'menu#loadMenu'
  get 'menu' => 'menu#index'
  get 'menu/loadMenu' => 'menu#loadMenu'

  post 'menu/addCart' => 'sessions#addToCart'
  post 'menu' => 'sessions#addToCart'
  post 'addCart' => 'sessions#addToCart'
  post 'removeProduct' => 'sessions#removeProductFromCart'
  post 'plusProduct' => 'sessions#plusProduct'
  post 'minusProduct' => 'sessions#minusProduct'
  post 'postOrder' => 'sessions#postOrder'


  controller :sessions do
    get 'login' => :new
    post 'login' => :create
    delete 'logout' => :destroy
  end

  #get 'admin/index'
  #get 'sessions/new'
  get 'sessions/create'
  get 'sessions/destroy'
  resources :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
